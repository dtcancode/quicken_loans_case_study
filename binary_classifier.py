import pandas as pd

import numpy as np

from tabulate import tabulate

from sklearn.feature_selection import RFE

from imblearn.over_sampling import SMOTE

from sklearn import preprocessing

from sklearn.model_selection import train_test_split

from sklearn.metrics import f1_score

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier

import sklearn.metrics as metrics

from sklearn.calibration import CalibratedClassifierCV

from sklearn.metrics import classification_report

import matplotlib.pyplot as plt

import seaborn as sns

file_directory = r"C:\Users\Daniel\PycharmProjects\quicken_loans_case_study_git\bank_data"

df_bank_data = pd.read_csv(file_directory + "\\" + "bank_additional_full.csv", sep=";",
                           encoding='ISO-8859-1', low_memory=False)
df_bank_data = df_bank_data.drop_duplicates()
# print(df_bank_data.info())

# check for imbalance
print(df_bank_data['y'].value_counts())

# find null values
# print(df_bank_data.isnull().sum())

# print(tabulate(df_bank_data.head(), headers='keys', tablefmt='psql'))

num = preprocessing.LabelEncoder()

num.fit(["admin.", "blue-collar", "entrepreneur", "housemaid", "management",
         "retired", "self-employed", "services", "student", "technician",
         "unemployed", "unknown"])
df_bank_data['job'] = num.transform(df_bank_data['job']).astype('int')

num.fit(["divorced", "married", "single", "unknown"])
df_bank_data['marital'] = num.transform(df_bank_data['marital']).astype('int')

num.fit(["basic.4y", "basic.6y", "basic.9y", "high.school", "illiterate",
         "professional.course", "university.degree", "unknown"])
df_bank_data['education'] = num.transform(df_bank_data['education']).astype('int')

num.fit(["no", "yes", "unknown"])
df_bank_data['default'] = num.transform(df_bank_data['default']).astype('int')

num.fit(["no", "yes", "unknown"])
df_bank_data['housing'] = num.transform(df_bank_data['housing']).astype('int')

num.fit(["no", "yes", "unknown"])
df_bank_data['loan'] = num.transform(df_bank_data['loan']).astype('int')

# num.fit(['cellular', 'telephone'])
# df_bank_data['contact'] = num.transform(df_bank_data['contact']).astype('int')
#
# num.fit(['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'])
# df_bank_data['month'] = num.transform(df_bank_data['month']).astype('int')
#
# num.fit(['mon', 'tue', 'wed', 'thu', 'fri'])
# df_bank_data['day_of_week'] = num.transform(df_bank_data['day_of_week']).astype('int')
#
# num.fit(["failure", "nonexistent", "success"])
# df_bank_data['poutcome'] = num.transform(df_bank_data['poutcome']).astype('int')

num.fit(["yes", "no"])
df_bank_data['y'] = num.transform(df_bank_data['y']).astype('int')

df_bank_data.drop(['duration', 'contact', 'month', 'day_of_week', 'duration', 'campaign',
                   'pdays', 'previous', 'poutcome'], axis=1, inplace=True)

X = np.asarray(df_bank_data[['age', 'job', 'marital', 'education', 'housing', 'loan',
                             'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m']])
y = np.asarray(df_bank_data['y'])

rfc = RandomForestClassifier(n_estimators=40)
rfe = RFE(rfc, 6)
rfe_fit = rfe.fit(X, y)

# sns.set(style="ticks")
#
# sns.pairplot(df_bank_data, hue="y")
# plt.show()

# Bringing balance to the frame
sm = SMOTE(sampling_strategy='auto')
X_sampled, y_sampled = sm.fit_sample(X, y)

Sampled_no = len(y_sampled[y_sampled == 0])
Sampled_yes = len(y_sampled[y_sampled == 1])
print([Sampled_no], [Sampled_yes])
# Perfectly balanced as all things should be

X_train, X_test, y_train, y_test = train_test_split(X_sampled, y_sampled, test_size=0.25, random_state=0)

# sns.set(style="ticks")
#
# sns.pairplot(df_bank_data, hue="y")
# plt.show()

# print(df_bank_data.dtypes)
# print(tabulate(df_bank_data.head(), headers='keys', tablefmt='psql'))

logistic_reg_model = LogisticRegression(C=1, solver='lbfgs')
support_vector_model = SVC(kernel='rbf', gamma='auto', probability=True)
decision_tree_model = DecisionTreeClassifier(criterion="entropy", max_depth=4)
random_forest_model = RandomForestClassifier(n_estimators=40)


def scorer(i, j, k, l):
    for every in (i, j, k, l):
        every.fit(X_train, y_train)
        print(every.__class__.__name__, 'F1 score =', f1_score(y_test, every.predict(X_test)))
        probs = every.predict_proba(X_test)
        preds = probs[:, 1]
        false_positive_rate, true_positive_rate, threshold = metrics.roc_curve(y_test, preds)
        roc_auc = metrics.auc(false_positive_rate, true_positive_rate)
        plt.title('Receiver Operating Characteristic' + ': ' + str(every).split('(')[0])
        plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f' % roc_auc)
        plt.legend(loc='lower right')
        plt.plot([0, 1], [0, 1], 'r--')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.show()


scorer(logistic_reg_model, decision_tree_model, random_forest_model, support_vector_model)


yhat = random_forest_model.predict(X_test)
print(classification_report(y_test, yhat))
