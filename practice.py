import pandas as pd

from sklearn.model_selection import train_test_split

from sklearn.linear_model import LogisticRegression

import matplotlib.pyplot as plt

import seaborn as sns

import numpy as np

from tabulate import tabulate

'''
We propose a data mining (DM) approach to predict the success of telemarketing calls for selling bank long-term
deposits. A Portuguese retail bank was addressed, with data collected from 2008 to 2013, thus including the effects
of the recent financial crisis. We analyzed a large set of 150 features related with bank client, product and
social-economic attributes. A semi-automatic feature selection was explored in the modeling phase, performed
with the data prior to July 2012 and that allowed to select a reduced set of 22 features. We also compared four
DM models: logistic regression, decision trees (DTs), neural network (NN) and support vector machine. Using
two metrics, area of the receiver operating characteristic curve (AUC) and area of the LIFT cumulative curve
(ALIFT), the four models were tested on an evaluation set, using the most recent data (after July 2012) and a
rolling window scheme. The NN presented the best results (AUC = 0.8 and ALIFT = 0.7), allowing to reach
79% of the subscribers by selecting the half better classified clients. Also, two knowledge extraction methods, a
sensitivity analysis and a DT, were applied to the NN model and revealed several key attributes (e.g., Euribor
rate, direction of the call and bank agent experience). Such knowledge extraction confirmed the obtained
model as credible and valuable for telemarketing campaign managers.
'''


file_directory = r"C:\Users\Daniel\PycharmProjects\quicken_loans_case_study_git\bank_data"

df_bank_data = pd.read_csv(file_directory + "\\" + "bank_additional_partial.csv", sep=";", encoding='ISO-8859-1', low_memory=False)
# df_bank_data = df_bank_data.drop_duplicates()

'''
variables: 'age', 'job', 'marital', 'education', 'default', 'balance',
       'housing', 'loan', 'contact', 'day', 'month', 'duration',
       'campaign', 'pdays', 'previous', 'poutcome'

classification: 'y'

'age', 'job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 
'month', 'day_of_week', 'duration', 'campaign', 'pdays', 'previous', 'poutcome', 
'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m', 'nr.employed', 'y' 


With given variables, determine probability of y.
'''

print(tabulate(df_bank_data, headers='keys', tablefmt='psql'))

print(tabulate(df_bank_data.head(), headers='keys', tablefmt='psql'))

# print(df_bank_data_partial.describe())

# bank_model = linear_model.LinearRegression()
# bank_model.fit(
#     df_bank_data_partial[['age', 'job', 'marital', 'education', 'default', 'balance',
#                           'housing', 'loan', 'contact', 'day', 'month', 'duration',
#                           'campaign', 'pdays', 'previous', 'poutcome']],
#     df_bank_data_partial.y)

'''
plt.figure(figsize=(8, 6))
Y = df_bank_data_partial["y"]
total = len(Y) * 1.
ax = sns.countplot(x="y", data=df_bank_data_partial)
for p in ax.patches:
    ax.annotate('{:.1f}%'.format(100 * p.get_height() / total), (p.get_x() + 0.1, p.get_height() + 5))

# put 11 ticks (therefore 10 steps), from 0 to the total number of rows in the dataframe
ax.yaxis.set_ticks(np.linspace(0, total, 11))
# adjust the ticklabel to the desired format, without changing the position of the ticks.
ax.set_yticklabels(map('{:.1f}%'.format, 100 * ax.yaxis.get_majorticklocs() / total))
ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
# ax.legend(labels=["no","yes"])
plt.show()
'''

corr = df_bank_data.corr()

f, ax = plt.subplots(figsize=(10, 12))

cmap = sns.diverging_palette(220, 10, as_cmap=True)

_ = sns.heatmap(corr, cmap="YlGn", square=True, ax=ax, annot=True, linewidth=0.1)

plt.title("Pearson correlation of Features", y=1.05, size=15)
# plt.show()

# print(df_bank_data.shape)
